package me.aanorbel.journalapp.views.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.data.model.Journal;
import me.aanorbel.journalapp.presenters.JournalListFragmentPresenter;
import me.aanorbel.journalapp.presenters.JournalListFragmentPresenterImpl;
import me.aanorbel.journalapp.presenters.JournalRecyclerAdapter;
import me.aanorbel.journalapp.views.JournalListFragmentView;

/**
 * A simple {@link Fragment} subclass.
 */
public class JournalListFragment extends Fragment implements JournalListFragmentView {

    private RecyclerView mRecyclerView;
    private JournalRecyclerAdapter mAdapter;

    private JournalListFragmentPresenter mPresenter;

    public JournalListFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new JournalListFragmentPresenterImpl();
        mPresenter.attachView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_journal_list, container, false);
        mRecyclerView = view.findViewById(R.id.rv_journal_list);
        mRecyclerView.setHasFixedSize(true);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.setUpRecyclerView(mRecyclerView,mAdapter);
        mPresenter.displayList();

    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Nullable
    @Override
    public Activity getContext() {
        return getActivity();
    }

}
