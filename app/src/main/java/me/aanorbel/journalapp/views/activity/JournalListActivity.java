package me.aanorbel.journalapp.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.base.BaseActivity;
import me.aanorbel.journalapp.presenters.JournalListActivityPresenter;
import me.aanorbel.journalapp.presenters.JournalListActivityPresenterImpl;
import me.aanorbel.journalapp.views.JournalListActivityView;

public class JournalListActivity extends BaseActivity implements JournalListActivityView {

    private JournalListActivityPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_list);
        mPresenter = new JournalListActivityPresenterImpl();
        mPresenter.attachView(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.journal_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void newJournal(View view) {
        mPresenter.newJournal();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
