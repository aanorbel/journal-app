package me.aanorbel.journalapp.presenters;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import me.aanorbel.journalapp.base.BasePresenter;
import me.aanorbel.journalapp.views.LoginActivityView;

public interface LoginActivityMvpPresenter extends BasePresenter<LoginActivityView> {

    void fireBaseAuthWithGoogle(GoogleSignInAccount acct);

    void signIn();

    void signOut();

    void revokeAccess();
}
