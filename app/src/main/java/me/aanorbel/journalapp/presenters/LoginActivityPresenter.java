package me.aanorbel.journalapp.presenters;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.views.LoginActivityView;

import static me.aanorbel.journalapp.views.activity.LoginActivity.RC_SIGN_IN;


public class LoginActivityPresenter implements LoginActivityMvpPresenter {
    private static final String TAG = LoginActivityPresenter.class.getSimpleName();
    private final FirebaseAuth mAuth;

    private GoogleSignInClient mGoogleSignInClient;
    private LoginActivityView view;


    public LoginActivityPresenter(FirebaseAuth mAuth) {
        this.mAuth = mAuth;
    }

    @Override
    public void fireBaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "fireBaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(view.getContext(), task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        view.loginSuccess(user);
                    } else {
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        view.loginError();
                    }

                });
    }


    @Override
    public void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        view.getContext().startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void attachView(LoginActivityView view) {
        this.view = view;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.view.getContext().getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this.view.getContext(), gso);
    }

    @Override
    public void detachView() {
        this.view = null;
    }


    public void signOut() {
        mAuth.signOut();

        mGoogleSignInClient.signOut().addOnCompleteListener(this.view.getContext(),
                task -> this.view.loginError());
    }

    public void revokeAccess() {
        mAuth.signOut();

        mGoogleSignInClient.revokeAccess().addOnCompleteListener(this.view.getContext(),
                task -> this.view.loginError());
    }

}
