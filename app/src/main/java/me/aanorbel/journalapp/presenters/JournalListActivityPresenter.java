package me.aanorbel.journalapp.presenters;

import me.aanorbel.journalapp.base.BasePresenter;
import me.aanorbel.journalapp.views.JournalListActivityView;

public interface JournalListActivityPresenter extends BasePresenter<JournalListActivityView> {

    void signOut();

    void newJournal();
}
