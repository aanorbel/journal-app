package me.aanorbel.journalapp.presenters;

import me.aanorbel.journalapp.base.BasePresenter;
import me.aanorbel.journalapp.views.NewJournalActivityView;

public interface NewJournalActivityPresenter  extends BasePresenter<NewJournalActivityView> {
    void submitJournal(String title, String body);
}
