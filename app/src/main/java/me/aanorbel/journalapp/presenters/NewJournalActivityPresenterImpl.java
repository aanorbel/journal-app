package me.aanorbel.journalapp.presenters;

import me.aanorbel.journalapp.data.DataManager;
import me.aanorbel.journalapp.views.NewJournalActivityView;

public class NewJournalActivityPresenterImpl implements NewJournalActivityPresenter {

    private NewJournalActivityView view;
    private DataManager dataManager;

    public NewJournalActivityPresenterImpl() {
        dataManager = DataManager.getInstance();
    }

    @Override
    public void attachView(NewJournalActivityView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void submitJournal(String title, String body) {
        dataManager.saveJournal(this.view.getContext(), title, body);
        view.getContext().finish();

    }
}
